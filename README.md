# petbook-react-chicago

Monorepo provides an example of utilization of Webpack Module Federation. It consist of multiple remote containers that are loaded asynchronously and dynamically. This is a demo that was shown as part of React Chicago Meetup. Slides and video of presentation you can find here: https://bitbucket.org/sskeirys/module-federation-react-chicago-resources

## Getting Started

Install dependencies:

```bash
yarn
```

Boostrap monorepo dependencies (installs all dependencies in `packages/*/` location):

```bash
yarn bootstrap
```

## Features

### Development mode

To run all hoists and remote containers, just run:

```bash
yarn start
```

It will start all applications and dashboard will be available at http://localhost:3000

### Build & Serve

To build into static application:

```bash
yarn build
```

To serve static application:

```bash
yarn serve
```
