const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const ModuleFederationPlugin =
  webpack.container.ModuleFederationPlugin;
const path = require("path");

const isProd = process.env.NODE_ENV === 'production';
console.log({isProd})
module.exports = {
  entry: "./src/index",
  mode: "development",
  devServer: {
    static: {
      directory: path.join(__dirname, "dist", 'v1'),
    },
    port: 3000,
    historyApiFallback: true
  },
  output: {
    publicPath: "auto",
    clean: true
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-react"],
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: { 
              name: '[name].[ext]?[hash]'
            }
          },
        ],
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "petbook_dashboard",
      shared: { react: { singleton: true }, "react-dom": { singleton: true } },
      remotes: isProd ? {
        petbook_components: "petbook_components@http://localhost:3001/0.0.0/remoteEntry.js",
        petbook_fans: "petbook_fans@http://localhost:3003/0.0.0/remoteEntry.js",
        petbook_friends: "petbook_friends@http://localhost:3004/0.0.0/remoteEntry.js",
      } : {
        petbook_components: "petbook_components@http://localhost:3001/remoteEntry.js",
        petbook_fans: "petbook_fans@http://localhost:3003/remoteEntry.js",
        petbook_friends: "petbook_friends@http://localhost:3004/remoteEntry.js",
      },
    }),
    new webpack.DefinePlugin({
      "env": JSON.stringify(isProd ? "production" : "local")
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: "./public/index.html"
    }),
  ],
};
