import moment from 'moment';

export default {
  name: 'Pablo',
  avatar: 'http://localhost:9000/images/avatar.jpg',
  images: [
    'http://localhost:9000/images/image1.jpg',
    'http://localhost:9000/images/image2.jpg',
    'http://localhost:9000/images/image3.jpg',
    'http://localhost:9000/images/image4.jpg',
  ],
  about: {
    content: 'Hi, my name is Pablo. I am 5 years old. Born in Klaipeda, Lithuania, but raised in Chicago, IL. I love getting belly rubs, being outside and playing fetch.'
  },
  fans: [{
    name: 'Charlie',
    avatar: 'http://localhost:9000/images/fan1.jpeg'
  },
  {
    name: 'Rudy',
    avatar: 'http://localhost:9000/images/fan2.jpeg'
  },
  {
    name: 'Kobe',
    avatar: 'http://localhost:9000/images/fan3.jpeg'
  },
  {
    name: 'Cash',
    avatar: 'http://localhost:9000/images/fan4.jpeg'
  },
  {
    name: 'Milo',
    avatar: 'http://localhost:9000/images/fan5.jpeg'
  },
  {
    name: 'Jasper',
    avatar: 'http://localhost:9000/images/fan6.jpeg'
  },
  {
    name: 'Bear',
    avatar: 'http://localhost:9000/images/fan7.jpeg'
  }],
  friends: [{
    name: 'Ryker',
    avatar: 'http://localhost:9000/images/ryker.png'
  },
  {
    name: 'Oliver',
    avatar: 'http://localhost:9000/images/oliver.png'
  },
  {
    name: 'Ophelia',
    avatar: 'http://localhost:9000/images/ophelia.png'
  },
  {
    name: 'Nuri',
    avatar: 'http://localhost:9000/images/nuri.png'
  }],
  comments: [
  {
    content: 'Let\'s go play outside!!!',
    datetime: moment().subtract(10, "minutes").fromNow()
  },{
    content: 'Happy to join PetBook!',
    datetime: moment().subtract(1, "days").fromNow()
  }],
  social: {
    instagram: 'pablo_the_cloud'
  }
}