import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  Link,
  useLocation
} from 'react-router-dom';

import Header from 'petbook_components/Header'
import Profile from 'petbook_components/Profile'
import Navigation from 'petbook_components/Navigation'
import Social from 'petbook_components/Social'
import Fans from 'petbook_fans/Fans';
import Friends from 'petbook_friends/Friends';
import {Row, Col, Layout} from 'petbook_components/default'
import store from './store';
import { dynamicImport } from './dynamicImport';
import './index.css'

// Lazy loading pages
const Wall = lazy(() => dynamicImport('petbook_wall/Wall'));
const Photos = lazy(() => dynamicImport('petbook_photos/Photos'));
const About = lazy(() => dynamicImport('petbook_about/About'));

const links = [{
  text: 'Home',
  route: '/home'
},
{
  text: 'About',
  route: '/about'
},
{
  text: 'Photos',
  route: '/photos'
}];

const Dashboard = () => {
  const { pathname } = useLocation();
  const { Content } = Layout;
  return(
    <Layout>
      <Header title="PetBook" />
      <Content>
        <div className="height-200" />
      </Content>
      <Content className="content">
        <div className="container">
          <Row>
            <Col span={5} offset={1}>
              <div className="section profile">
                <Profile src={store.avatar} alt={store.name} />
              </div>
              <div className="section">
                <Social instagram={store.social.instagram} />
              </div>
              <div className="section">
                <Fans fans={store.fans} />
              </div>
            </Col>
            <Col span={11} offset={1}>
              <div className="navigation margin-bottom-20">
                <Navigation links={links} Link={Link} selectedPage={pathname} />
              </div>
              <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                  <Route exact path="/">
                    <Redirect to="/home" />
                  </Route>
                  <Route path="/home">
                    <div className="section">
                      <Wall 
                        name={store.name}
                        avatar={store.avatar}
                        comments={store.comments}
                      />
                    </div>
                  </Route>
                  <Route path="/about">
                    <div className="section">
                      <About 
                        content={store.about.content}
                        social={store.social}
                      />
                    </div>
                  </Route>
                  <Route path="/photos">
                    <div className="section">
                      <Photos images={store.images} />
                    </div> 
                  </Route>
                </Switch>
              </Suspense>
            </Col>
            <Col span={5} offset={1}>
              <div className="section">
                <Friends friends={store.friends} />
              </div>
            </Col>
          </Row>
        </div>
      </Content>
    </Layout>
  );
};

const withRouter = () => <Router><Dashboard /></Router>;

ReactDOM.render(withRouter(), document.getElementById("root"));
