import React, { useEffect } from 'react';
import Social from 'petbook_components/Social'
import {Typography} from 'petbook_components/default'
import uuid from 'uuid';

const dependencies = require('../package.json').dependencies;

const fireTrackingPixel = () => {
  const trackingPixel = uuid.v4();
  console.log('About.fireTrackingPixel', { 
    uuid: dependencies.uuid,
    value: trackingPixel
  })
}

const About = ({ content, social }) => {
  const { Paragraph, Title } = Typography;

  useEffect(() => {
    fireTrackingPixel()
  },[]);

  return (
    <div>
      <Title level={4}>About</Title>
      <Paragraph>
        {content}
      </Paragraph>
      <Social instagram={social.instagram} />
    </div>
  )
}
  

export default About;