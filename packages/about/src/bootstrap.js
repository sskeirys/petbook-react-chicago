import React from "react";
import ReactDOM from "react-dom";
import About from './About';

const App = () => 
  <About 
    content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
    social={{
      instagram: 'account_name'
    }} />
  
ReactDOM.render(<App />, document.getElementById("root"));
