import React from "react";
import ReactDOM from "react-dom";
import moment from 'moment';
import Wall from './Wall';

const App = () => 
  <Wall
    name="Author Name"
    avatar={require('./images/fan1.jpeg').default}
    comments={
      [
        {
          content: 'My second post.',
          datetime: moment().subtract(1, "minutes").fromNow()
        },{
          content: 'My first post.',
          datetime: moment().subtract(1, "days").fromNow()
        }
      ]
    } />
  
ReactDOM.render(<App />, document.getElementById("root"));
