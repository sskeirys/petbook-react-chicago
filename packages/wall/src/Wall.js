import React, { useEffect } from 'react';
import { v4 as uuid } from 'uuid';
import Post from 'petbook_components/Post'

const dependencies = require('../package.json').dependencies;

const fireTrackingPixel = () => {
  const trackingPixel = uuid();
  console.log('Wall.fireTrackingPixel', { 
    uuid: dependencies.uuid,
    value: trackingPixel
  })
}

const Wall = ({ name, avatar, comments }) => {
  useEffect(() => {
    fireTrackingPixel()
  },[]);

  return comments ? comments.map((comment, idx) => (
      <Post key={idx} avatar={avatar} name={name} datetime={comment.datetime} content={comment.content} />
    )) : null;
}
  

export default Wall;