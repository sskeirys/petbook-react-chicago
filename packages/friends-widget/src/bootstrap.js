import React from "react";
import ReactDOM from "react-dom";
import Friends from './Friends';

const App = () => 
  <Friends 
    friends={[{
      name: 'Charlie',
      avatar: require(`./images/fan1.jpeg`).default
    }]} />;
  
ReactDOM.render(<App />, document.getElementById("root"));
