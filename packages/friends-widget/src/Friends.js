import React from 'react';
import Box from 'petbook_components/Box'
import {Row, Col} from 'petbook_components/default'
import Avatar from 'petbook_components/Avatar'
import './Friends.css';

const Friends = ({friends}) => 
  <Box title="Friends">
    <Row>
      {friends && friends.map((friend, idx) => (
        <Col key={idx} className="friend-avatar" >
          <Avatar src={friend.avatar} alt={friend.name} />
        </Col>
      ))}
    </Row>
  </Box>

export default Friends;