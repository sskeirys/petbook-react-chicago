const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin =
  require("webpack").container.ModuleFederationPlugin;
const path = require("path");
const packageJson = require("./package.json");

module.exports = {
  entry: "./src/index",
  mode: "development",
  devServer: {
    static: {
      directory: path.join(__dirname, "dist"),
    },
    port: 3004,
    historyApiFallback: true
  },
  output: {
    path: path.join(__dirname, "dist", packageJson.version),
    chunkFilename: '[name].chunk.js',
    filename: '[name].bundle.js',
    publicPath: "auto"
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-react"],
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: { 
              name: '[name].[ext]?[hash]'
            }
          },
        ],
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "petbook_friends",
      filename: "remoteEntry.js",
      shared: { react: { singleton: true }, "react-dom": { singleton: true } },
      remotes: {
        petbook_components: "petbook_components@http://localhost:3001/remoteEntry.js",
      },
      exposes: {
        "./Friends": "./src/Friends"
      },
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
  ],
};
