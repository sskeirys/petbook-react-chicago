import React from 'react';
import Box from 'petbook_components/Box'
import {Row, Col} from 'petbook_components/default'
import Avatar from 'petbook_components/Avatar'
import './Fans.css';

const Fans = ({fans}) => 
  <Box title="Fans">
    <Row>
      {fans && fans.map((fan, idx) => (
        <Col key={idx} className="fans-avatar" >
          <Avatar src={fan.avatar} alt={fan.name} />
        </Col>
      ))}
    </Row>
  </Box>

export default Fans;