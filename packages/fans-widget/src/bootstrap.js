import React from "react";
import ReactDOM from "react-dom";
import Fans from './Fans';

const App = () => 
  <Fans 
    fans={[{
      name: 'Charlie',
      avatar: require(`./images/fan1.jpeg`).default
    }]} />;
  
ReactDOM.render(<App />, document.getElementById("root"));
