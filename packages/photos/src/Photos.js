import React, { useEffect } from 'react';
import {Row, Col, Image} from 'petbook_components/default'
import { v4 as uuid } from 'uuid';
import './Photos.css';

const dependencies = require('../package.json').dependencies;

const fireTrackingPixel = () => {
  const trackingPixel = uuid();
  console.log('About.fireTrackingPixel', { 
    uuid: dependencies.uuid,
    value: trackingPixel
  })
}

const Photos = ({ images }) => {
  useEffect(() => {
    fireTrackingPixel()
  },[]);

  return (
    <Row>
      {images && images.map(((image, idx) => (
        <Col key={idx}>
          <div  className="image">
            <Image
              width={100}
              height={100}
              src={image}
            />
            </div>
        </Col>
      )))}
    </Row>
  )
}
  

export default Photos;