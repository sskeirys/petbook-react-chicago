import React from "react";
import ReactDOM from "react-dom";
import Photos from './Photos';

const image = require('./images/fan1.jpeg').default;

const App = () => 
  <Photos images={[image,image,image]} />
  
ReactDOM.render(<App />, document.getElementById("root"));
