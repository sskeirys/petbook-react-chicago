const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin =
  require("webpack").container.ModuleFederationPlugin;
const path = require("path");
const packageJson = require("./package.json");
const deps = packageJson.dependencies;

module.exports = {
  entry: "./src/index",
  mode: "development",
  devServer: {
    static: {
      directory: path.join(__dirname, "dist"),
    },
    port: 3005,
    historyApiFallback: true
  },
  output: {
    path: path.join(__dirname, "dist", packageJson.version),
    chunkFilename: '[name].chunk.js',
    filename: '[name].bundle.js',
    publicPath: "auto"
  },
  optimization: {
    chunkIds: 'named',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-react"],
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: { 
              name: '[name].[ext]?[hash]'
            }
          },
        ],
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "petbook_photos",
      filename: "remoteEntry.js",
      shared: { 
        react: { 
          singleton: true
        }, 
        "react-dom": { 
          singleton: true 
        },
        uuid: deps.uuid
        // uuid: {
        //   // requiredVersion: deps.react,
        //   // import: "react", // the "react" package will be used a provided and fallback module
        //   shareKey: "uuid", // under this name the shared module will be placed in the share scope
        //   shareScope: "default", // share scope with this name will be used
        //   // strictVersion: true
        //   // singleton: true, // only a single version of the shared module is allowed
        // }
      },
      remotes: {
        petbook_components: "petbook_components@http://localhost:3001/remoteEntry.js",
      },
      exposes: {
        "./Photos": "./src/Photos"
      },
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
  ],
};
