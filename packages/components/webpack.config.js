const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin =
  require("webpack").container.ModuleFederationPlugin;
const path = require("path");
const packageJson = require("./package.json");

module.exports = {
  entry: "./src/index",
  mode: "development",
  devServer: {
    static: {
      directory: path.join(__dirname, "dist"),
    },
    port: 3001,
  },
  output: {
    path: path.join(__dirname, "dist", packageJson.version),
    chunkFilename: '[name].chunk.js',
    filename: '[name].bundle.js',
    publicPath: "auto"
  },
  optimization: {
    chunkIds: 'named',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-react"],
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "petbook_components",
      filename: "remoteEntry.js",
      shared: { react: { singleton: true }, "react-dom": { singleton: true } },
      exposes: {
        "./Avatar": "./src/Avatar",
        "./Box": "./src/Box",
        "./Button": "./src/Button",
        "./Header": "./src/Header",
        "./Header.css": "./src/Header.css",
        "./Navigation": "./src/Navigation",
        "./Profile": "./src/Profile",
        "./Social": "./src/Social",
        "./Post": "./src/Post",
        "./default": './src/antd'
      },
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
  ],
};
