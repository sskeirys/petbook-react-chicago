import React from "react";
import ReactDOM from "react-dom";
import Navigation from './Navigation';
import Avatar from './Avatar';
import Box from './Box';
import Profile from './Profile';
import Header from './Header';
import Social from './Social';
import Post from './Post';
import moment from "moment";

import 'antd/dist/antd.css';
require('./index.css');

const links = [{
  text: 'link one',
  selected: true,
  route: '/link-one'
},
{
  text: 'link two',
  selected: false,
  route: '/link-two'
},
{
  text: 'link three',
  selected: false,
  route: '/link-three'
}];

const App = () => {
  const dogImage = 'http://loremflickr.com/320/240/dog,puppy';
  const datetime = moment().subtract(15, "minutes").fromNow();
  return (
    <div className="container">
      <h1>PETBOOK Design System</h1>
      <div>
        <h2>Header</h2>
        <Header title="PETBOOK" />
      </div>
      <div>
        <h2>Navigation</h2>
        <Navigation links={links} />
      </div>
      <div>
        <h2>Avatar</h2>
        <Avatar src={dogImage} alt="Dog Name" />
      </div>
      <div>
        <h2>Box</h2>
        <div className="box-section">
          <Box title="Section">Content</Box>
        </div>
      </div>
      <div>
        <h2>Profile</h2>
        <Profile src={dogImage} alt="Dog Name" />
      </div>
      <div>
        <h2>Social</h2>
        <Social instagram="account_name" />
      </div>
      <div>
        <h2>Post</h2>
        <Post name="Dog Name" avatar={dogImage} content="hi" datetime={datetime} />
      </div>
    </div>
  )
}
  
ReactDOM.render(<App />, document.getElementById("root"));
