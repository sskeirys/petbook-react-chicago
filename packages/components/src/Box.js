import React from 'react';
import { Typography } from 'antd';
import 'antd/dist/antd.css';
import './Box.css'

const Box = ({title, children = 'Placeholder'}) => {
  const { Title } = Typography;
  return (
    <div className="box-wrapper">
      <Title level={4} className="box-header">{title}</Title>
      <div className="box-content">
        {children}
      </div>
    </div>
  );
}

export default Box;