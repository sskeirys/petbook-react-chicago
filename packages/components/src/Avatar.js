import React from 'react';
import { Avatar as AvatarRaw, Tooltip } from 'antd';
import 'antd/dist/antd.css';

const Avatar = ({ src, alt }) => 
  <Tooltip title={alt} placement="top">
    <AvatarRaw 
      alt={alt}
      size={48}
      src={src} />
  </Tooltip>

export default Avatar;

