import React from 'react';
import { Layout } from 'antd';
import 'antd/dist/antd.css';
import './Header.css';
// require('./Header.css');

const Header = ({ title, href }) => {
  const { Header } = Layout;
  return(
    <Header className="header">
      <div className="wrapper">
        <h2>{title}</h2>
      </div>
    </Header>
  )
}

export default Header;