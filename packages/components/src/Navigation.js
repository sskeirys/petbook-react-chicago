import React from 'react';
import { Button, Space } from 'antd';
import 'antd/dist/antd.css';
import './Navigation.css';

const Navigation = ({ links, Link, selectedPage }) => {
  if (!links || links.length === 0) return null;
  return (
    <Space>
      {links && links.map(link => {
        const { text, route } = link;

        if (!Link) return <Button type={route === selectedPage ? 'primary' : 'default'}>{text}</Button>;
        return (
          <Link key={route} to={route}>
            <Button type={route === selectedPage ? 'primary' : 'default'} className="button">{text}</Button>
          </Link>
        )
      })}
      </Space>
  );
};
  

export default Navigation;