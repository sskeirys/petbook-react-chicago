import React from 'react';
import { Row, Col, Typography } from 'antd';
import 'antd/dist/antd.css';
import './Post.css';
import Avatar from './Avatar';

const Post = ({ avatar = 'http://loremflickr.com/320/240/dog,puppy', name, content, datetime }) => {
  const { Title, Paragraph, Text } = Typography;

  return (
    <div className="post">
      <Row className="margin-bottom-10">
        <Col>
          <Avatar src={avatar} />
          <Title level={5} className="author">{name}</Title>
          <Text disabled>{datetime}</Text>
        </Col>
      </Row>
      <Row>
        <Col>
          <Paragraph>
            {content}
          </Paragraph>
        </Col>
      </Row>
    </div>
  )
}

export default Post;