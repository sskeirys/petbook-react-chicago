import React from 'react';
import { Avatar } from 'antd';
import 'antd/dist/antd.css';

const Profile = ({ src = 'http://loremflickr.com/320/240/dog,puppy', alt }) => 
  <Avatar shape="square" size={200} src={src} alt={alt} />
  

export default Profile;