import React from 'react';
import { Typography } from 'antd';
import Button from './Button';
import {
  InstagramOutlined
} from '@ant-design/icons';
import 'antd/dist/antd.css';

const Social = ({ instagram }) => {
  const { Title } = Typography;
  return(
    <div>
      <Title level={4}>Social</Title>
      {instagram && 
        <>
          <InstagramOutlined />
          <Button type="link" href={`https://instagram.com/${instagram}`} target="_blank">@{instagram}</Button>
        </>}
    </div>
  )
}

export default Social;